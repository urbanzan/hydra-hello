{
  description = "Hydra Hello project";

  inputs = { nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable"; };

  outputs = { self, nixpkgs }:
    let
      system = "aarch64-linux";
      pkgs = import nixpkgs {
        inherit system;
        overlays =
          [ (final: prev: { hello = import ./default.nix { pkgs = prev; }; }) ];
      };
    in {
      #packages.x86_64-linux."hello_b" = pkgs.hello.hello_b;
      #packages.x86_64-linux."hello_c" = pkgs.hello.hello_c;
      #packages.x86_64-linux."hello_cpp" = pkgs.hello.hello_cpp;
      #packages.x86_64-linux."hello_rust" = pkgs.hello.hello_rust;

      packages.aarch64-linux."hello_c_aarch64" = pkgs.hello.hello_c;
      packages.aarch64-linux."hello_cpp_aarch64" = pkgs.hello.hello_cpp;
      packages.aarch64-linux."hello_rust_aarch64" = pkgs.hello.hello_rust;

      #hydraJobs."hello_b" = self.packages.x86_64-linux."hello_b";
      #hydraJobs."hello_c" = self.packages.x86_64-linux."hello_c";
      #hydraJobs."hello_cpp" = self.packages.x86_64-linux."hello_cpp";
      #hydraJobs."hello_rust" = self.packages.x86_64-linux."hello_rust";

      hydraJobs."hello_c_aarch64" = self.packages.aarch64-linux."hello_c_aarch64";
      hydraJobs."hello_cpp_aarch64" = self.packages.aarch64-linux."hello_cpp_aarch64";
      hydraJobs."hello_rust_aarch64" = self.packages.aarch64-linux."hello_rust_aarch64";
    };
}
