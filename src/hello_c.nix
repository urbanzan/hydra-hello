{ pkgs ? import <nixpkgs> { } }:

pkgs.stdenv.mkDerivation {
  name = "hello_c";
  src = ./.;
  nativeBuildInputs = with pkgs; [ gcc ];
  buildPhase = "gcc -o hello_c ./hello.c";
  installPhase = "mkdir -p $out/bin; install -t $out/bin hello_c";
}
