{ pkgs ? import <nixpkgs> { } }:

pkgs.stdenv.mkDerivation {
  name = "hello_rust";
  src = ./.;
  nativeBuildInputs = with pkgs; [ cargo rustc ];
  buildPhase = "rustc ./hello.rs -o hello_rust";
  installPhase = "mkdir -p $out/bin; install -t $out/bin hello_rust";
}
